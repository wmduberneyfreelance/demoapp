import { Component } from '@angular/core';
import { MenuController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { Page1Page } from '../page1/page1';
import { Page2Page } from '../page2/page2';

@Component({
  selector: 'page-home-tabs',
  templateUrl: 'home-tabs.html',
})
export class HomeTabsPage {

  tab1Root: any = HomePage;
  tab2Root: any = Page1Page;
  tab3Root: any = Page2Page;

  constructor(private menuCtrl: MenuController) {
  	this.menuCtrl.enable(true);
  }

}
