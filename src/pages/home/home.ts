import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { Page1Page } from '../page1/page1';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  modalComments: any = null;

  constructor(public navCtrl: NavController, 
              public modalCtrl: ModalController) {

  }

  loadCommentsPage(){
    this.modalComments = this.modalCtrl.create(Page1Page, { 'comments': 'demo', 'idContent': "demo2" } );
    this.modalComments.present();

    this.modalComments.onDidDismiss( parametros => {
      if (parametros){
        console.log (parametros);
      }
    })
  }

}
